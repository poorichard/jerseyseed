package mybatis;


import static org.junit.Assert.assertEquals;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hulu.bean.auth.User;
import com.hulu.mybatis.UserMapper;
import com.hulu.util.HuluSessionFactory;

public class TestSqlFactory{

	@Test
	public void TestUserMapper(){
		UserMapper um = session.getMapper(UserMapper.class);
        User user = um.getUserById(1);
        assertEquals("root",user.getUser_name());
        user = um.getUserById(2);
        assertEquals("user",user.getUser_name());
        
        List<User> userList = um.getUserList();
        assertEquals(2,userList.size());
        assertEquals("root",userList.get(0).getUser_name());
        assertEquals("user",userList.get(1).getUser_name());
	}
	
	SqlSession session;
	
	@Before
	public void setup(){
		session = HuluSessionFactory.getDevSessionFactory().openSession();
	}
	
	@After
	public void teardown(){
		session.close();
	}
	
}
