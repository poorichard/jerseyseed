package com.hulu.rest;

import com.hulu.bean.auth.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;

/**
 * Created by comstar on 2017/3/21.
 */
public class UserResourceTest {

    private static String serverUri = "http://localhost:8080/JerseySeed/rest";
    private static Logger logger = LoggerFactory.getLogger(UserResourceTest.class);

    public static void main(String[] args) {
        insertUser();
    }

    private static void insertUser() {
        logger.info("****insertUser****");
        User user = new User(101,"Hulu","showmtthepwd","User","Y","ae1ef1f4f5d77dba0826643375f59771",new Date());
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(serverUri + "/users/insertUser");
        Response response = target.request().buildPost(Entity.entity(user, MediaType.APPLICATION_JSON)).invoke();
        response.close();
    }

}