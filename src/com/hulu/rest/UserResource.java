package com.hulu.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hulu.bean.system.ReturnType;
import com.hulu.bean.auth.User;
import com.hulu.mybatis.UserMapper;
import com.hulu.util.HuluSessionFactory;

/**
 * @author Richard
 */
@Path("/users")
public class UserResource {

	private Logger logger = LoggerFactory.getLogger(UserResource.class);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<User> getAllUsers() {
		logger.info("we are logged: getAllUsers");
		try {
			SqlSession session = HuluSessionFactory.getDevSessionFactory().openSession();
			UserMapper um = session.getMapper(UserMapper.class);
			return um.getUserList();
		} catch (Exception e) {
			logger.error(e.getMessage());
			return null;
		}
	}

	@GET
	@Path("/user/{i}")
	@Produces(MediaType.APPLICATION_JSON)
	public User getUserById(@PathParam("i") String i) {
		logger.info("we are logged: getUserById");
		try {
			SqlSession session = HuluSessionFactory.getDevSessionFactory().openSession();
			UserMapper um = session.getMapper(UserMapper.class);
			return um.getUserById(Integer.parseInt(i));
		} catch (Exception e) {
			logger.error(e.getMessage());
			return null;
		}
	}
	
	@POST
	@Path("/insertUser")
	@Consumes(MediaType.APPLICATION_JSON)
	public ReturnType insertUser(User user) {
		logger.info("we are logged: insertUser");
		try {
			SqlSession session = HuluSessionFactory.getDevSessionFactory().openSession();
			UserMapper um = session.getMapper(UserMapper.class);
			int r =  um.insertUser(user);
			if(r > 0){
				logger.info("insert succeeded:"+r);
				session.commit();
				return new ReturnType(true,"Succeeded","");
			}else{
				logger.info("insert failed");
				return new ReturnType(false,"Failed","F001");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			return null;
		}
	}
	
	@POST
	@Path("/insertUser2/{id}/{name}")
	@Consumes(MediaType.TEXT_PLAIN)
	public ReturnType insertUser2(@PathParam("id") String id,@PathParam("name") String name) {
		logger.info("we are logged: insertUser2");
		try {
			User user = new User();
			user.setUser_name(name);
			user.setUser_pwd("showmethepwd");
			user.setUser_role("user");
			user.setStatus("Y");
			user.setSalt("salt");
			SqlSession session = HuluSessionFactory.getDevSessionFactory().openSession();
			UserMapper um = session.getMapper(UserMapper.class);
			int r =  um.insertUser(user);
			if (r <= 0) {
				logger.info("insert failed");
				return new ReturnType(false,"Failed","F001");
			} else {
				logger.info("insert succeeded:"+r);
				session.commit();
				return new ReturnType(true,"Succeeded","");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			return null;
		}
	}

}
