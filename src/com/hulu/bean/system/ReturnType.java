package com.hulu.bean.system;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement
public class ReturnType {
	
	/**
	 * 服务器端操作数据库成功与否
	 */
	private boolean status = false;
	/**
	 * 服务器端返回给页面的消息
	 */
	private String msg;
	/**
	 * 错误代码
	 */
	private String errorCode;
	

}
