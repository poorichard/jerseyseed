package com.hulu.bean.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by comstar on 2017/3/21.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement
public class Role {

    private int role_id;

    private String role_name;

    private String note;
}
