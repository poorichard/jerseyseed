package com.hulu.bean.auth;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement
public class User {
	
    private int user_id;

    private String user_name;

    private String user_pwd;

    private String user_role;

    private String status;
    
    private String salt;

    private Date upd_time;
}
