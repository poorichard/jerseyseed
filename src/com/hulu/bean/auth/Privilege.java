package com.hulu.bean.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by comstar on 2017/3/22.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement
public class Privilege {

    private int privilege_id;

    private String privilege_name;

    private String note;
}
