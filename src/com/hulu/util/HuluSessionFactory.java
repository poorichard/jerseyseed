package com.hulu.util;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Richard
 */
public class HuluSessionFactory {

	private static Logger logger = LoggerFactory.getLogger(HuluSessionFactory.class.getName());
	
	public static SqlSessionFactory getDevSessionFactory(){
		return getSqlSessionFactory(DEV);
	}
	
    private final static String MYBATIS_CONFIG = "mybatis-config.xml" ;
   
    public final static String DEV = "dev" ;
   
    public static SqlSessionFactory getSqlSessionFactory(String environment){
        InputStream inputStream;
        SqlSessionFactory sqlSessionFactory = null ;
        try {
            inputStream = Resources.getResourceAsStream(MYBATIS_CONFIG);
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream , environment);
            inputStream.close();
            logger.info("get [ " +environment + " ] datasource success");
        } catch (IOException e) {
            logger.error("get [ " +environment + " ] datasource fail，error ：" + e);
        }
        return sqlSessionFactory ;
    }
    
}
