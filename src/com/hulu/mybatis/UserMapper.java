package com.hulu.mybatis;

import java.util.List;

import com.hulu.bean.auth.User;

/**
 * @author Richard
 */
public interface UserMapper{

	public User getUserById(int user_id);

	public List<User> getUserList();

	public int insertUser(User user);
}
