# 说明
## app

   * rest service 在 com.hulu.rest 目录下，是实现业务逻辑的地方 
   * 配置文件在 WebContent/WEB_IF/web.xml
   * 部署时，打包为war, 在tomcat中启动，测试tomcat版本为7.0 
   * 测试server地址 为  
     * 获取用户列表                http://localhost:8080/JerseySeed/rest/users  
     * 根据用户编号获取用户  http://localhost:8080/JerseySeed/rest/users/user/2
   * POST
     * 提交数据时一般是用POST方法，如果是GET方法，不符合规范，但实际上应该也行
     * POST方法有两种
		1. 用JSon或XML格式, 如UserResource中的insertUser，测试文件在test/rest/TestUserPost
		   测试地址： http://localhost:8080/JerseySeed/rest/insertUser
		1. 用多参数法，如UserResource中的insertUser2，参数很多时，链接会变得很长，一般推荐用第一种
		  测试地址：  http://localhost:8080/JerseySeed/rest/insertUser2/{id}/{name}
		
## Database

	* MySql 5.7
	* 测试数据在database目录下
	* 和DB交互使用Mybatis, 配置文件为 resource/mybatis-config.xml
	 
