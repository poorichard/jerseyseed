INSERT INTO user (user_name, user_pwd, user_role, status, salt)
VALUES ('root', 'showmethepwd', 'root', 'Y', 'ae1ef1f4f5d77dba0826643375f59771');
INSERT INTO user (user_name, user_pwd, user_role, status, salt)
VALUES ('user', 'showmethepwd', 'user', 'Y', 'ae1ef1f4f5d77dba0826643375f59771');

INSERT INTO role (role_name, note)
VALUES ('admin', 'administration');
INSERT INTO role (role_name, note)
VALUES ('user', 'default user');

INSERT INTO privilege (privilege_name, note)
VALUES ('auth_config', 'crud user,role,privilege');
INSERT INTO privilege (privilege_name, note)
VALUES ('default_query', 'default query');

INSERT INTO user_role (user_id, role_id, upd_user)
VALUES (1, 1, 1);
INSERT INTO user_role (user_id, role_id, upd_user)
VALUES (2, 2, 1);

INSERT INTO role_privilege (role_id, privilege_id, upd_user)
VALUES (1, 1, 1);
INSERT INTO role_privilege (role_id, privilege_id, upd_user)
VALUES (1, 2, 1);
INSERT INTO role_privilege (role_id, privilege_id, upd_user)
VALUES (2, 2, 1);

COMMIT;