CREATE TABLE user_role (
  user_id  INTEGER NOT NULL,
  role_id  INTEGER NOT NULL,
  upd_user INTEGER NOT NULL,
  upd_time TIMESTAMP DEFAULT current_timestamp ON UPDATE current_timestamp,
  PRIMARY KEY (user_id, role_id)
);