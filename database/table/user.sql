CREATE TABLE user (
  user_id       INTEGER      NOT NULL auto_increment,
  user_name     VARCHAR(64)  NOT NULL,
  user_pwd      VARCHAR(128) NOT NULL,
  user_role     VARCHAR(32)  NOT NULL,
  salt     VARCHAR(64),
  status   VARCHAR(12),
  upd_time TIMESTAMP DEFAULT current_timestamp ON UPDATE current_timestamp,
  primary key(user_id)
);