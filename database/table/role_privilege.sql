CREATE TABLE role_privilege (
  role_id      INTEGER NOT NULL,
  privilege_id INTEGER NOT NULL,
  upd_user     INTEGER NOT NULL,
  upd_time     TIMESTAMP DEFAULT current_timestamp ON UPDATE current_timestamp,
  PRIMARY KEY (role_id, privilege_id)
);