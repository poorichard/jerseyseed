CREATE TABLE role (
  role_id       INTEGER      NOT NULL auto_increment,
  role_name     VARCHAR(64)  NOT NULL,
  note     VARCHAR(128),
  upd_time TIMESTAMP DEFAULT current_timestamp ON UPDATE current_timestamp,
  PRIMARY KEY (role_id)
);