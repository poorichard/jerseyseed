CREATE TABLE privilege (
  privilege_id       INTEGER      NOT NULL auto_increment,
  privilege_name     VARCHAR(64)  NOT NULL,
  note     VARCHAR(128),
  upd_time TIMESTAMP DEFAULT current_timestamp ON UPDATE current_timestamp,
  PRIMARY KEY (privilege_id)
);